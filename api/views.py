from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework import mixins
from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes, action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_200_OK

from .models import Image as ImageClass
from .permissions import IsPostOrIsAuthenticated
from .serializers import ImageSerializer, UserSerializer


class ImageViewSet(viewsets.GenericViewSet,
                   mixins.CreateModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   mixins.DestroyModelMixin
                   ):
    queryset = ImageClass.objects.all().order_by('id')
    serializer_class = ImageSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.owner == request.user.id:
            instance.delete()
            return Response({'status': 'image deleted'}, status=status.HTTP_204_NO_CONTENT)
        return Response({'status': 'Unauthorizerd action'}, status=status.HTTP_401_UNAUTHORIZED)


class CreateRetrieveListUserViewSet(viewsets.GenericViewSet,
                                    mixins.CreateModelMixin,
                                    mixins.RetrieveModelMixin,
                                    mixins.ListModelMixin,
                                    mixins.UpdateModelMixin, ):
    permission_classes = [IsPostOrIsAuthenticated]
    model = User
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def get_queryset(self):
        return User.objects.filter(id=self.request.user.id)

    @action(detail=False)
    def images(self, request):
        queryset = ImageClass.objects.filter(owner=self.request.user.id)
        serializer = ImageSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)


@api_view(["POST"])
@permission_classes((AllowAny,))
def auth(request):
    username = request.data.get("username")
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Username and password needed'},
                        status=HTTP_400_BAD_REQUEST)
    user = authenticate(username=username, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({'token': token.key},
                    status=HTTP_200_OK)
