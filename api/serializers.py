__author__ = "Miha"
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from rest_framework.validators import UniqueValidator
from versatileimagefield.serializers import VersatileImageFieldSerializer
from rest_framework import serializers
from . import models


class ImageSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.id')
    img = VersatileImageFieldSerializer(
        sizes=[
            ('full_size_after', 'thumbnail__1920x1080'),
            ('thumbnail', 'crop__500x500'),
        ]
    )

    class Meta:
        model = models.Image
        fields = ['id', 'description', 'timestamp', 'owner', 'url', 'img']
        read_only_fields = ['id', 'timestamp', 'owner', 'url', 'thumbnail', 'img']


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(write_only=True)
    images = serializers.HyperlinkedRelatedField(many=True, view_name='image-detail', read_only=True)

    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data['email'],
            username=validated_data['username'],
            password=make_password(validated_data['password'])
        )
        user.save()
        return user

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'password', 'date_joined', 'url', 'images']
        read_only_fields = ['id', 'date_joined', 'url', 'images']
