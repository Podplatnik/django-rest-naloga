from django.db import models
from versatileimagefield.fields import VersatileImageField, PPOIField


class Image(models.Model):
    description = models.CharField(max_length=100, default="Slika")
    timestamp = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey('auth.User', related_name='images', on_delete=models.CASCADE)
    img = VersatileImageField(
        'img',
        upload_to='uploads/',
        ppoi_field='img_ppoi',
        blank=True
    )

    """
    Primary Point of Interest vezan za morebitno rezanje, ki pa ni iz centra slike
    """
    img_ppoi = PPOIField()

    def __str__(self):
        return "%s" % self.description
