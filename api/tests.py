from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase


class SelfieAPITestCase(APITestCase):
    def setUp(self):
        user = User(username='admin', email='test@test.com')
        user.set_password("admin")
        user.save()

    def test_token(self):
        response = self.client.post('http://127.0.0.1:8000/auth/', {'username': 'admin', 'password': 'admin'}, format='json')
        self.assertTrue(response.json().get('token'), True)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        self.client.credentials(HTTP_AUTHORIZATION='token {}'.format(response.json().get('token')))
        response = self.client.get('http://127.0.0.1:8000/user/', )
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response.json().get('count'), 1)

    def test_registration(self):
        response = self.client.post('http://127.0.0.1:8000/user/', {'username': 'user', 'password': 'user', 'email': 'ghet@test.com'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


