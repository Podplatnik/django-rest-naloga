# Django-RESTFull API - naloga

## Odprte točke

* [Registracija] : `POST /user/ - potreben username, password in email`
* [Login - Get Token] : `POST /auth/ - potreben username in password` 
* [Začetna stran routerja] : `GET /` 
* [Vse slike] : `GET /images/`
## Točke, ki potrebujejo avtentikacijo (Header z Authentication: 'token <token_key>')

* [Uporabnik] : `GET /user/`
* [Slike uporabnika] : `GET /user/images/`
* [Upload slike] : `POST /images/ - potreben img(slika) in description`
 
 Vsepovsod so možnosti OPTIONS in HEAD, na nekaterih DELETE in PUT/PATCH

### Navodila
- pripravimo si novi, friški virtualEnv (virtualenv <ime>),
- si ga aktiviramo (activate <ime>),
- pridobimo vse potrebne knjižnice (pip install -r requirements.txt)
- migracije: 
python manage.py makemigrations
python manage.py migrate
python manage.py migrate --run-syncdb
python manage.py runserver <port>
- kreacija superuserja po želji (python manage.py createsuperuser)